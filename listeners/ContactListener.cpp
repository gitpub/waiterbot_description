
// ROS includes.
#include "ros/ros.h"
#include "waiterbot_description/Contact.h"

// Include gazebo
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>

#include <iostream>
#include <chrono>

bool DEBUG = false;

std::chrono::steady_clock::time_point previous = std::chrono::steady_clock::now();

std::unordered_set<std::string> current_collisions;

/*--------------------------------------------------------------------
 * messageCallback()
 * Main callback for the msgs received
 *------------------------------------------------------------------*/

void messageCallback(ConstContactsPtr &_contacts)
{
  std::chrono::steady_clock::time_point next = std::chrono::steady_clock::now();
  double elapsed = std::chrono::duration_cast<std::chrono::microseconds>(next - previous).count();

  if (elapsed >= 0.3 * 1000000)
  {
    std::unordered_set<std::string> collisions;
    for (int i = 0; i < _contacts->contact_size(); i++)
    {
      std::string collision1 = _contacts->contact(i).collision1();
      collisions.insert(collision1);
    }
    current_collisions = collisions;

    if (DEBUG)
    {
      ROS_INFO("CL: There are %d collisions", (int)collisions.size());

      for (const std::string &x : collisions)
      {
        ROS_INFO("CL: Object %s", x.c_str());
      }
    }

    previous = std::chrono::steady_clock::now();
  }
}

bool sensorCheck(waiterbot_description::Contact::Request &req, waiterbot_description::Contact::Response &res)
{
  res.tray_loaded = !current_collisions.empty();
  return true;
}

/*--------------------------------------------------------------------
 * main()
 * Main function to set up ROS node.
 *------------------------------------------------------------------*/

int main(int argc, char **argv)
{
  if (argc < 4)
  {
    ROS_ERROR("RC: You MUST specify the robot ID");
    return 1;
  }

  int ROBOT_ID = atoi(argv[1]);

  // Set up ROS.
  ros::init(argc, argv, "contact_listener_" + std::to_string(ROBOT_ID));
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("contact_server_" + std::to_string(ROBOT_ID), sensorCheck);

  // Set up Gazebo
  gazebo::client::setup(argc, argv);
  gazebo::transport::NodePtr node(new gazebo::transport::Node());
  node->Init();
  gazebo::transport::SubscriberPtr gazSub = node->Subscribe("/wt_contact_topic", messageCallback);

  ROS_INFO("CL: Contact listener started");

  while (ros::ok())
  {
    gazebo::common::Time::MSleep(10);
    ros::spinOnce();
  }

  gazebo::client::shutdown();
  return 0;
}