#include "ros/ros.h"
#include "ros/package.h"
#include "waiterbot_description/Bartender.h"

#include "gazebo_msgs/SpawnModel.h"

#include "../include/position.h"

#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <queue>
#include <fstream>

ros::ServiceClient spawnClient;

int getRandomSeconds();
bool loadTray(waiterbot_description::Bartender::Request &req, waiterbot_description::Bartender::Response &res);
void spawnOrder(int table_id, Position pos);

std::map<int, Position> map_positions;

std::string readModel();

int main(int argc, char **argv)
{
    map_positions[1] = makePosition(2.631144, 2.864968, 3.3, 0, 0, 0, 0);
    map_positions[2] = makePosition(2.631144, 2.264968, 3.3, 0, 0, 0, 0);

    srand(time(NULL));
    ros::init(argc, argv, "bartender_server");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("bartender", loadTray);
    spawnClient = n.serviceClient<gazebo_msgs::SpawnModel>("gazebo/spawn_urdf_model");

    ROS_INFO("BS: Ready to load trays.");

    while (ros::ok())
    {
        ros::spinOnce();
    }

    return 0;
}

int getRandomSeconds()
{
    return rand() % 10 + 1;
}

bool loadTray(waiterbot_description::Bartender::Request &req, waiterbot_description::Bartender::Response &res)
{
    int robot_id = req.robot_id;
    int table_id = req.table_id;

    ROS_INFO("BS: Loading order on robot n.%d", robot_id);
    spawnOrder(table_id, map_positions[robot_id]);

    return true;
}

/**
 * string model_name
 * string model_xml
 * string robot_namespace
 * geometry_msgs/Pose initial_pose
 *   geometry_msgs/Point position
 *     float64 x
 *     float64 y
 *     float64 z
 *   geometry_msgs/Quaternion orientation
 *     float64 x
 *     float64 y
 *     float64 z
 *     float64 w
 * string reference_frame
 * ---
 * bool success
 * string status_message
 **/

void spawnOrder(int table_id, Position pos)
{
    gazebo_msgs::SpawnModel srv;
    srv.request.model_name = "order_table_" + std::to_string(table_id);
    srv.request.model_xml = readModel();
    srv.request.initial_pose.position.x = pos.px;
    srv.request.initial_pose.position.y = pos.py;
    srv.request.initial_pose.position.z = pos.pz;
    srv.request.initial_pose.orientation.x = 0;
    srv.request.initial_pose.orientation.y = 0;
    srv.request.initial_pose.orientation.z = 0;
    srv.request.initial_pose.orientation.w = 0;
    srv.request.reference_frame = "world";

    if (spawnClient.call(srv))
    {
        if ((bool)srv.response.success)
        {
            ROS_INFO("BS: Spawn riuscito: %s", ((std::string)srv.response.status_message).c_str());
        }
        else
        {
            ROS_INFO("BS: Spawn non riuscito: %s", ((std::string)srv.response.status_message).c_str());
        }
    }
    else
    {
        ROS_ERROR("RC: Failed to call service gazebo/spawn_urdf_model");
    }
}

std::string readModel()
{
    std::string ads = "waiterbot_gazebo";
    std::string packagePath = ros::package::getPath(ads);
    std::string fileString = "";
    std::string line;

    std::ifstream file(packagePath + "/objects/cylinder.urdf.xacro");
    while (!file.eof())
    {
        std::getline(file, line);
        fileString += line;
    }
    file.close();

    return fileString;
}