#include "ros/ros.h"
#include "waiterbot_description/Order.h"
#include "waiterbot_description/Contact.h"

#include "gazebo_msgs/DeleteModel.h"
#include "std_msgs/String.h"

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"

#include "actionlib_msgs/GoalStatusArray.h"

#include "../include/position.h"

#include <cstdlib>
#include <chrono>
#include <map>

std::chrono::steady_clock::time_point previous = std::chrono::steady_clock::now();

enum class States
{
    SIMULATION_STOP,
    WAITING_ORDER,
    LOADING_TRAY,
    SHIPPING_ORDER,
    WAITING_PICKUP,
    RETURNING_STATION,
    CHARGING,
    ERROR
};

enum class Goals
{
    CHARGING_STATION_1,
    CHARGING_STATION_2,
    TABLE_1,
    TABLE_2,
    TABLE_3,
};

const Goals tables_map[] = {Goals::TABLE_1, Goals::TABLE_2, Goals::TABLE_3};
const Goals stations_map[] = {Goals::CHARGING_STATION_1, Goals::CHARGING_STATION_2};

std::map<Goals, Position> map_positions;

ros::ServiceClient kitchenClient;
ros::ServiceClient contactClient;
ros::ServiceClient deleteClient;
ros::Publisher goalPublisher;
ros::Publisher initialposePublisher;

int ROBOT_ID = -1;
int SERVING_TABLE = -1;
States CURRENT_STATUS = States::SIMULATION_STOP;

int getOrder();
bool isTrayLoaded();
void deleteOrder(int table_id);
void gotoGoal(Position pos);
void setInitialPose(Position pos);

void checkGoalCallback(const actionlib_msgs::GoalStatusArray &msg);
void startsimulationCallback(const std_msgs::String::ConstPtr &msg);

int main(int argc, char **argv)
{
    map_positions[Goals::CHARGING_STATION_1] = makePosition(2.540, 2.780, 0, 0, 0, -1, 0);
    map_positions[Goals::CHARGING_STATION_2] = makePosition(2.540, 2.180, 0, 0, 0, -1, 0);
    map_positions[Goals::TABLE_1] = makePosition(-1.98767, 3.46194, 0, 0, 0, -1, 0);
    map_positions[Goals::TABLE_2] = makePosition(-0.97677, 1.22974, 0, 0, 0, -0.70735, 0.70735);
    map_positions[Goals::TABLE_3] = makePosition(2.41512, 0.76660, 0, 0, 0, 0, 1);

    if (argc < 4)
    {
        ROS_ERROR("RC: You MUST specify the robot ID");
        return 1;
    }

    ROBOT_ID = atoi(argv[1]);

    ros::init(argc, argv, "robot_controller_" + std::to_string(ROBOT_ID));
    ROS_INFO("RC: Robot controller n.%d started", ROBOT_ID);

    ros::NodeHandle n;
    kitchenClient = n.serviceClient<waiterbot_description::Order>("kitchen_orders");
    contactClient = n.serviceClient<waiterbot_description::Contact>("contact_server_" + std::to_string(ROBOT_ID));
    deleteClient = n.serviceClient<gazebo_msgs::DeleteModel>("gazebo/delete_model");

    ros::Subscriber navigationStatusSubscriber = n.subscribe("/move_base/status", 1000, checkGoalCallback);
    ros::Subscriber startSimulationSubscriber = n.subscribe("/waiterbot/start_simulation", 1000, startsimulationCallback);

    goalPublisher = n.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1000, true);
    initialposePublisher = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1000, true);

    if (!goalPublisher)
    {
        ROS_ERROR("RC: Goal Publisher not connected");
    }
    if (!initialposePublisher)
    {
        ROS_ERROR("RC: InitialPose Publisher not connected");
    }

    setInitialPose(makePosition(0.3, 3.4, 0, 0, 0, 0, 1));

    while (ros::ok())
    {
        std::chrono::steady_clock::time_point next = std::chrono::steady_clock::now();
        double elapsed = std::chrono::duration_cast<std::chrono::microseconds>(next - previous).count();

        if (CURRENT_STATUS == States::WAITING_ORDER && elapsed >= 500000) //0.5
        {
            int table_id = getOrder();
            if (table_id != -1)
            {
                ROS_INFO("RC: Robot n.%d recevived an order for table %d", ROBOT_ID, table_id);
                SERVING_TABLE = table_id;
                CURRENT_STATUS = States::LOADING_TRAY;
            }
            previous = std::chrono::steady_clock::now();
        }

        if (CURRENT_STATUS == States::LOADING_TRAY && elapsed >= 500000) //0.5
        {
            if (isTrayLoaded())
            {
                CURRENT_STATUS = States::SHIPPING_ORDER;
                ROS_INFO("RC: Robot n.%d started the order shipping to table %d", ROBOT_ID, SERVING_TABLE);
                gotoGoal(map_positions[tables_map[SERVING_TABLE]]);
            }
            else
            {
                ROS_INFO("RC: Robot n.%d is waiting for tray loading", ROBOT_ID);
            }
            previous = std::chrono::steady_clock::now();
        }

        if (CURRENT_STATUS == States::SHIPPING_ORDER && !isTrayLoaded())
        {
            ROS_INFO("RC: Robot n.%d lost his order while shipping", ROBOT_ID);
            CURRENT_STATUS = States::ERROR;
        }

        if (CURRENT_STATUS == States::SHIPPING_ORDER) //30
        {
            // TODO: IMPLEMENT NAVIGATION
            bool arrived = true;
            if (arrived)
            {
            }
            previous = std::chrono::steady_clock::now();
        }

        if (CURRENT_STATUS == States::WAITING_PICKUP && !isTrayLoaded())
        {
            ROS_INFO("RC: Robot n.%d shipped the order correctly", ROBOT_ID);
            CURRENT_STATUS = States::RETURNING_STATION;
            gotoGoal(map_positions[stations_map[ROBOT_ID - 1]]);
            ROS_INFO("RC: Robot n.%d returning to the station", ROBOT_ID);
        }

        if (CURRENT_STATUS == States::WAITING_PICKUP && isTrayLoaded() && elapsed >= 5000000) //5
        {
            deleteOrder(SERVING_TABLE);
            previous = std::chrono::steady_clock::now();
        }

        ros::spinOnce();
    }

    return 0;
}

int getOrder()
{
    waiterbot_description::Order srv;
    srv.request.robot_id = ROBOT_ID;
    if (kitchenClient.call(srv))
    {
        return (int)srv.response.table_id;
    }
    else
    {
        ROS_ERROR("RC: Failed to call service kitchen_orders");
        return -1;
    }
}

bool isTrayLoaded()
{
    waiterbot_description::Contact srv;
    if (contactClient.call(srv))
    {
        return (bool)srv.response.tray_loaded;
    }
    else
    {
        ROS_ERROR("RC: Failed to call service contact_server_%d", ROBOT_ID);
        return false;
    }
}

/**
 * string model_name
 * ---
 * bool success
 * string status_message
 **/

void deleteOrder(int table_id)
{
    gazebo_msgs::DeleteModel srv;
    srv.request.model_name = "order_table_" + std::to_string(table_id);
    if (deleteClient.call(srv))
    {
        if ((bool)srv.response.success)
        {
            ROS_INFO("BS: Spawn riuscito: %s", ((std::string)srv.response.status_message).c_str());
        }
        else
        {
            ROS_INFO("BS: Spawn non riuscito: %s", ((std::string)srv.response.status_message).c_str());
        }
    }
    else
    {
        ROS_ERROR("RC: Failed to call service gazebo/spawn_urdf_model");
    }
}

/**
 * std_msgs/Header header
 *   uint32 seq
 *   time stamp
 *   string frame_id
 * geometry_msgs/Pose pose
 *   geometry_msgs/Point position
 *     float64 x
 *     float64 y
 *     float64 z
 *   geometry_msgs/Quaternion orientation
 *     float64 x
 *     float64 y
 *     float64 z
 *     float64 w  
 **/
void gotoGoal(Position pos)
{
    geometry_msgs::PoseStamped msg;

    msg.header.frame_id = "map";
    msg.pose.position.x = pos.px;
    msg.pose.position.y = pos.py;
    msg.pose.position.z = pos.pz;
    msg.pose.orientation.x = pos.ox;
    msg.pose.orientation.y = pos.oy;
    msg.pose.orientation.z = pos.oz;
    msg.pose.orientation.w = pos.ow;

    goalPublisher.publish(msg);
}

void setInitialPose(Position pos)
{
    geometry_msgs::PoseWithCovarianceStamped msg;

    msg.header.frame_id = "map";
    msg.pose.pose.position.x = pos.px;
    msg.pose.pose.position.y = pos.py;
    msg.pose.pose.position.z = pos.pz;
    msg.pose.pose.orientation.x = pos.ox;
    msg.pose.pose.orientation.y = pos.oy;
    msg.pose.pose.orientation.z = pos.oz;
    msg.pose.pose.orientation.w = pos.ow;
    msg.pose.covariance.fill(0.0);

    initialposePublisher.publish(msg);
}

void checkGoalCallback(const actionlib_msgs::GoalStatusArray &msg)
{
    int lenght = msg.status_list.size();
    // ROS_INFO("RC: %d %d", msg.status_list[lenght - 1].status, lenght);
    if (lenght > 0 && msg.status_list[lenght - 1].status == 3)
    {
        if (CURRENT_STATUS == States::SHIPPING_ORDER)
        {
            ROS_INFO("RC: Robot n.%d arrived to the table", ROBOT_ID);
            CURRENT_STATUS = States::WAITING_PICKUP;
        }

        if (CURRENT_STATUS == States::RETURNING_STATION)
        {
            ROS_INFO("RC: Robot n.%d returned to the station", ROBOT_ID);
            CURRENT_STATUS = States::WAITING_ORDER;
        }
    }
}

void startsimulationCallback(const std_msgs::String::ConstPtr &msg)
{
    ROS_INFO("RC: Robot n.%d simulation started", ROBOT_ID);
    CURRENT_STATUS = States::RETURNING_STATION;
    gotoGoal(map_positions[stations_map[ROBOT_ID - 1]]);
    // ros::Duration(1).sleep();
}