#include "ros/ros.h"
#include "waiterbot_description/Order.h"
#include "waiterbot_description/Bartender.h"

#include "std_msgs/String.h"

#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <queue>

std::chrono::steady_clock::time_point previous = std::chrono::steady_clock::now();
std::queue<int> orders;
std::set<int> available_tables;

ros::ServiceClient bartenderClient;

int NUM_OF_TABLES = 3;
int SIMULATION_STARTED = false;

int getRandomSeconds();
bool sendOrder(
    waiterbot_description::Order::Request &req,
    waiterbot_description::Order::Response &res);
void sendOrderToBartender(int robot_id, int table_id);

void startsimulationCallback(const std_msgs::String::ConstPtr &msg);

int main(int argc, char **argv)
{
  srand(time(NULL));
  ros::init(argc, argv, "kitchen_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("kitchen_orders", sendOrder);

  ros::Subscriber startSimulationSubscriber = n.subscribe("/waiterbot/start_simulation", 1000, startsimulationCallback);

  ROS_INFO("KS: Ready to spawn kitchen orders.");

  bartenderClient = n.serviceClient<waiterbot_description::Bartender>("bartender");

  int next_order_seconds = getRandomSeconds() * 1000000;

  while (ros::ok())
  {
    if (SIMULATION_STARTED)
    {
      std::chrono::steady_clock::time_point next = std::chrono::steady_clock::now();
      double elapsed = std::chrono::duration_cast<std::chrono::microseconds>(next - previous).count();

      if (elapsed >= next_order_seconds)
      {
        int next_table = rand() % NUM_OF_TABLES;
        orders.push(next_table);
        ROS_INFO("KS: New order for table n.%d", next_table);

        next_order_seconds = getRandomSeconds() * 1000000;
        ROS_INFO("KS: Next order in %d seconds", next_order_seconds / 1000000);

        previous = std::chrono::steady_clock::now();
      }
    }

    ros::spinOnce();
  }

  return 0;
}

int getRandomSeconds()
{
  return rand() % 20 + 5;
}

bool sendOrder(waiterbot_description::Order::Request &req, waiterbot_description::Order::Response &res)
{
  int robot_id = req.robot_id;
  ROS_INFO("KS: Request from robot n.%d", robot_id);

  int table_id = -1;
  if (!orders.empty())
  {
    table_id = orders.front();
    orders.pop();
    ROS_INFO("KS: Sending order to table n.%d", table_id);
    sendOrderToBartender(robot_id, table_id);
  }
  else
  {
    ROS_INFO("KS: No order yet recived");
  }

  res.table_id = table_id;
  return true;
}

void sendOrderToBartender(int robot_id, int table_id)
{
  waiterbot_description::Bartender srv;
  srv.request.robot_id = robot_id;
  srv.request.table_id = table_id;
  if (!bartenderClient.call(srv))
  {
    ROS_ERROR("KS: Failed to call service bartender");
  }
}

void startsimulationCallback(const std_msgs::String::ConstPtr &msg)
{
  ROS_INFO("KS: Simulation started ");
  SIMULATION_STARTED = true;
}