#ifndef MY_POSITIONS_H
#define MY_POSITIONS_H

typedef struct
{
    double px;
    double py;
    double pz;
    double ox;
    double oy;
    double oz;
    double ow;
} Position;
Position makePosition(double px, double py, double pz, double ox, double oy, double oz, double ow)
{
    Position str = {px, py, pz, ox, oy, oz, ow};
    return str;
}

#endif /* MY_POSITIONS_H */